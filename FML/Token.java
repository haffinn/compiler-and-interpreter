package FML;

public class Token
{    
	public enum TokenCode 
	{ 
		ID, ASSIGN, SEMICOL, INT, PLUS, MINUS, MULT, LPAREN, RPAREN, PRINT, ERROR, END
	}

    String lexeme;
    TokenCode tCode;

    Token(String myLexeme, TokenCode myTCode)
    {
        lexeme = myLexeme;
        tCode = myTCode;
    }
}
