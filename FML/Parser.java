package FML;

import java.io.IOException;

//import FML.Token.TokenCode;

public class Parser 
{
	private Lexer myLex;	 
	private Token myTok;
	
	
	//Error messages.
	private final String err = "Syntax error!";
	
	//A constructor for the class Parser.
	Parser(Lexer lex)
	{	
		myLex = lex;
	}
	
	private void print(String str)
	{ 
		System.out.println(str);
		
		if(str == err)
		{
			System.exit(1);
		}
	}

	//A function that reprisents "Statements" in the context free grammar
	//We are allowed to call statement, semi colon statments or we get the end token.
	private void Statements() throws IOException
	{
		//Statment ';' Staments
		if(myTok.tCode != Token.TokenCode.END)
		{	
			Statement();
			if (myTok.tCode == Token.TokenCode.SEMICOL)
			{
                myTok = myLex.nextToken();
				Statements();
            }
			else
			{
				print(err);
			}
		}
		else 
		{
			//The end token is executed! 
			return;
		}
	}

	//The Statment function. Represents eather the 'id = ' Expr or 'print id'
	private void Statement() throws IOException
	{
		//id=Expr
		if (myTok.tCode == Token.TokenCode.ID)
		{
			print("PUSH " + myTok.lexeme);
			myTok = myLex.nextToken();
			if (myTok.tCode == Token.TokenCode.ASSIGN)
			{
				myTok = myLex.nextToken();
				Expr();
				print("ASSIGN");
			}
			else
			{
				print(err);
			}
		}//print id
		else if (myTok.tCode == Token.TokenCode.PRINT)
		{
			myTok = myLex.nextToken();
			if (myTok.tCode == Token.TokenCode.ID)
			{
				print("PUSH " + myTok.lexeme);
				print("PRINT");
				myTok = myLex.nextToken();
			}
			else
			{
				print(err);
			}
		}
		else
		{
			print(err);
		}
	}
	// Expr which allows us Term | Term + Expr | Term - Expr.
	private void Expr() throws IOException 
	{
		//Term
		Term();
		
		//Term + Expr
		if (myTok.tCode == Token.TokenCode.PLUS)
		{
			myTok = myLex.nextToken();
			Expr();
			print("ADD");
		}
        //Term - Expr
		else if (myTok.tCode == Token.TokenCode.MINUS)
        {
            myTok = myLex.nextToken();
            Expr();
            print("SUB");
        }
	}
	//Term -> Factor | Factor * Term
	private void Term() throws IOException
	{	
		//Factor
		Factor();
		//Factor * Term
		if (myTok.tCode == Token.TokenCode.MULT)
		{
			myTok = myLex.nextToken();
			Term();
			print("MULT");
		}
	}
	// Factor -> 'int' | 'id' " '(' Expr ')'
	private void Factor() throws IOException 
	{
		//int
		if (myTok.tCode == Token.TokenCode.INT) 
		{
			print("PUSH " + myTok.lexeme);
			myTok = myLex.nextToken();

		}
		//id
		else if(myTok.tCode == Token.TokenCode.ID)
		{
			print("PUSH " + myTok.lexeme);
			myTok = myLex.nextToken();
		}
		//(Expr)
		else if (myTok.tCode == Token.TokenCode.LPAREN)
		{
			myTok = myLex.nextToken();
			Expr();
			if (myTok.tCode == Token.TokenCode.RPAREN)
			{
				myTok = myLex.nextToken();
			}
			else
			{
				print(err);
			}
		}
		else
		{
			print(err);
		}
	}

	public void parse() throws IOException
	{
		myTok = myLex.nextToken();
		Statements();
	}

}