//
//  main.cpp
//  Interpreter
//
//  Created by Hafþór Snær Þórsson on 18/02/15.
//  Copyright (c) 2015 Hafþór Snær Þórsson. All rights reserved.
//

#include <iostream>
#include <map>
#include <string>
#include <stack>


using namespace std;
    
//class interpreter {
//public:
//    string quantity;
//    string operation;
//};

void calculator(string opera, stack<string>& sta)
{
    if (sta.empty())
    {
        return;
    }
    else
    {
        string val2 = sta.top();
        sta.pop();
        string val1 = sta.top();
        sta.pop();
        
        int intval1 = stoi(val1);
        int intval2 = stoi(val2);
        int result = 0;
        
        if (opera == "ADD")
        {
            result = intval1 + intval2;
        }
        else if (opera == "SUB")
        {
            result = intval1 - intval2;
        }
        else if (opera == "MULT")
        {
            result = intval1 * intval2;
        }
        string res = to_string((long long)result);
        sta.push(res);
    }
    
    return;
}

void assign (stack<string>& sta, map<string, int>& ma)
{
    if (sta.empty())
    {
        return;
    }
    else
    {
        string value = sta.top();
        sta.pop();
        string name = sta.top();
        sta.pop();
        
        int val = stoi(value);
        ma[name] = val;
    }
    
    return;
}

//void printFunc(stack<string>& sta, map<string, int>& ma)
//{
//    if (!sta.empty())
//    {
//        string name = sta.top();
//        int value = ma[name];
//        cout << value << endl;
//    }
//}

int main()
{
    string input;
    
    stack<string> myStack;
    map<string, int> myMap;
    
    while(getline(cin, input))
    {
        string quantity = "";
        string operation = "";
        for (size_t i = 0; i < input.size(); i++)
        {
            if (input[i] == ' ')
            {
                for (size_t j = 0; j < i; j++)
                {
                    operation.push_back(input[j]);
                }
                for (size_t j = i + 1; j < input.size(); j++)
                {
                    quantity.push_back(input[j]);
                }
            }
        }
        if (input == "ADD" || input == "SUB" || input == "MULT")
        {
            calculator(input, myStack);
        }
        else if (input == "ASSIGN")
        {
            assign(myStack, myMap);
        }
        else
        {
//            string quantity = "";
//            string operation = "";
//            for (size_t i = 0; i < input.size(); i++)
//            {
//                if (input[i] == ' ')
//                {
//                    for (size_t j = 0; j < i; j++)
//                    {
//                        operation.push_back(input[j]);
//                    }
//                    for (size_t j = i + 1; j < input.size(); j++)
//                    {
//                        quantity.push_back(input[j]);
//                    }
//                }
//            }
            if (operation == "PUSH" && quantity != "")
            {
                if (isalpha(quantity[0]))
                {
                    if (myMap.count(quantity))
                    {
                        int value = myMap[quantity];
                        string val = to_string((long long)value);
                        myStack.push(val);
                    }
                    else
                    {
                        myStack.push(quantity);
                    }
                }
                else
                {
                    myStack.push(quantity);
                }
            }
            else if (input == "PRINT")
            {
//                printFunc(myStack, myMap);
                cout << myStack.top() << endl;

            }
            else
            {
                cout << "Error for operator: " << operation << endl;
            }
        }
        
//        while (!myStack.empty())
//        {
//            std::cout << ' ' << myStack.top();
//            myStack.pop();
//        }
    }
    
    
    return 0;
}
