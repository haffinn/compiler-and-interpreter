package FML;
import java.io.IOException;
import java.util.Vector;

//import FML.Token.TokenCode;

public class Lexer 
{
	private char lastLetter;
	private boolean notEnd = true;
	
	//Function that takes the input and determines if it is a id or a int
	//The function adds the input to a vector.
	private void sortInput(char myChar, Vector<Character> lex) throws IOException
	{
		 if(Character.isDigit(myChar))
		 {
			while (Character.isDigit(myChar))
			{
				lex.add(myChar);
				myChar = (char) System.in.read();	
			}
		 }
		 else if (Character.isLetter(myChar))
		 {
			 while (Character.isLetter(myChar))
				{
					lex.add(myChar);
					myChar = (char) System.in.read();
					
				}
		 }
		 
		 notEnd = false;
		 lastLetter =  myChar;
		 
	}

	//A function that finds the correct token and adds it to the a vector.
	private Token correctToken(char myChar, Vector<Character> lex) 
	{
		if(myChar == '(')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.LPAREN);	
		}
		else if(myChar == ')')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.RPAREN); 
		}
		else if(myChar == '=')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.ASSIGN);
		}
		else if(myChar == ';')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.SEMICOL);
		}
		else if(myChar == '+')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.PLUS);
		}
		else if(myChar == '-')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.MINUS);
		}
		else if(myChar == '*')
		{
			lex.add(myChar);
			return new Token(lex.toString(), Token.TokenCode.MULT);
		}
		else
		{	
			lex.add(myChar);
			return new Token("", Token.TokenCode.ERROR);
		}
	}

	// A function that turns the vector to a string.
	private String vecToStr(Vector<Character> myVec)
	{
		String myVecToString = "";
		for(Character c : myVec)
		{
			myVecToString += c;
		}
		return myVecToString;
	}
	
	
	public Token nextToken() throws IOException
	{
		char myChar;
       	Vector<Character> lexeme = new Vector<Character>();
       	myChar = notEnd ? (char) System.in.read() : lastLetter; 
       	
		if(Character.isWhitespace(myChar))
		{
			while(Character.isWhitespace(myChar))
			{
				myChar = (char) System.in.read();
			}
		}
		sortInput(myChar, lexeme);
        	
        if (vecToStr(lexeme).equals("end"))
        {
        	return new Token(vecToStr(lexeme), Token.TokenCode.END);
        }
        else if (vecToStr(lexeme).equals("print"))
        {
        	return new Token(vecToStr(lexeme), Token.TokenCode.PRINT);
        }
        else if(Character.isDigit(myChar))
        {
        	return new Token(	);
        }
        else if(Character.isLetter(myChar))
        {
        	return new Token(vecToStr(lexeme), Token.TokenCode.ID);
        }
        else
        {
        	notEnd = true;
        	return correctToken(myChar, lexeme);
       }
   }
}